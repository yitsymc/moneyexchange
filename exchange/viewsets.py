from rest_framework import viewsets
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.decorators import action
import requests
import json
from django.utils import timezone

class ExchangeViewSet(viewsets.ModelViewSet):
    queryset = Exchange.objects.all()
    serializer_class = ExchangeSerializer

    @action(detail=False, methods=['post'])
    def convert(self, request, *args, **kwargs):
        """
        Method POST '/convert'
        :param currency: Currency with ISO3 format ( EUR, USD, BTC)
        :param amount: amount passed by user in float type (Ex: 12 must be passed as 12.0)
        :return: status, 200 if ok, 400 if the currency not found
        """
        currency = request.POST.get('currency','')
        amount = float(request.POST.get('amount',0))
        api_objects = ApiKey.objects.all()
        api_key = api_objects[0].key if api_objects else ''
        url_response = requests.get(
            "https://openexchangerates.org/api/latest.json?app_id={}".format(api_key)).json()
        if url_response.get('status', 200) != 200:
            return Response(json.dumps({"status": url_response['status'],
                                        "message": url_response['message']}, indent=4))
        all_currencies = [rate for rate in url_response['rates']]
        # Find the passed currency in data, if not return a 404 status and a message
        if currency.upper() not in all_currencies:
            return Response(json.dumps({"status": 400, "message": "Currency not found"}))

        # Find currency rate and save into db
        find_currency = url_response['rates'][currency.upper()]
        final_amount = find_currency * amount
        Exchange.objects.create(requested_currency = currency.upper(), requested_amount = amount,
                                currency_change = find_currency, result_amount = round(final_amount, 8),
                                time_request = timezone.now())
        return Response(json.dumps({'status': 200}))

    def destroy(self, request, pk=None):
        pass

    @action(detail=False, methods=['get'])
    def last_currency(self, request, *args, **kwargs):
        queryset = self.filter_queryset(Exchange.objects.all())
        if request._request.GET:
            currency = request._request.GET.get('currency','')
            count = request._request.GET.get('count', 0)
            if currency:
                queryset = self.filter_queryset(
                    Exchange.objects.filter(requested_currency = currency.upper()))
            if count:
                queryset = queryset[0:int(count)]
        else:
            queryset = queryset[0:1]
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ApiKeyViewSet(viewsets.ModelViewSet):
    queryset = ApiKey.objects.all()
    serializer_class = ApiKeyTypeSerializer

    # def create(self, request):
    #     ApiKey.objects.all().delete()
    #     serializer = ApiKeyTypeSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     return Response(serializer.data)