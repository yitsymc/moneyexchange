from django.db import models
from django.utils import timezone

class Exchange(models.Model):

    requested_amount = models.DecimalField(verbose_name="Requested Amount", default=0.00, decimal_places=8, max_digits=15)
    requested_currency = models.CharField(verbose_name="Requested Currency", max_length=3)
    currency_change = models.DecimalField(verbose_name="Currency Check", default=0.00, decimal_places=8, max_digits=15)
    result_amount = models.DecimalField(verbose_name="Result Amount", default=0.00, decimal_places=8, max_digits=15)
    time_request = models.DateTimeField(verbose_name="Time Request", default=timezone.now())

    class Meta:
        ordering = ('-time_request',)


class ApiKey(models.Model):

    key = models.CharField(verbose_name="Open Exchange Rates API Key", max_length=50)

    class Meta:
        ordering = ('-id',)