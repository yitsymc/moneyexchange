from rest_framework import routers
from exchange.viewsets import *


router = routers.DefaultRouter()
router.register(r'exchangeRequest', ExchangeViewSet)
router.register(r'apikey', ApiKeyViewSet)
