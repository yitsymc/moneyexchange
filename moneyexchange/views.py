from django.views.generic import TemplateView

class IndexPage(TemplateView):
    template_name = 'index.html'


class RequestPage(TemplateView):
    template_name = 'request.html'