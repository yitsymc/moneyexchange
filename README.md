# MoneyExchange

Web API (http: // SERVER: 8000 / api /) developed in Django to convert currencies to USD, use the api of https://openexchangerates.org to obtain the change in real time. 
You must specify a key, for tests use "e72815fd9bd848eb8e36dda34ce48390".
**CURL Call Samples:**
*New API KEY"*
curl -X POST -d "key=e72815fd9bd848eb8e36dda34ce48390" http://localhost:8000/api/apikey/

*New Currency Conversion*
curl -X POST -d "currency=eur&amount=535.78" http://localhost:8000/api/exchangeRequest/convert/
Return:

*  "{\n    \"status\": 401,\n    \"message\": \"invalid_app_id\"\n}" - You need to specified a valid API KEY, the api always use the last key saved.


*  "{\"status\": 200}" - OK


*  "{\"status\": 400, \"message\": \"Currency not found\"}" - You need to specified a valid currency using ISO3 format (ex. EUR, BRL,...)


*Get last saved conversion (Always return a list of exchange objects):*


*  Without parameters return the last conversion:
curl -X GET http://localhost:8000/api/exchangeRequest/last_currency/
[{"id":42,"requested_amount":"53.98000000","requested_currency":"EUR","currency_change":"0.89035000","result_amount":"48.06109300","time_request":"2019-07-07T15:19:21.221882Z"}]


*  Specified the last conversions of certain currency:
curl -X GET http://localhost:8000/api/exchangeRequest/last_currency/?currency=cup


*  Specified the number of last conversions:
curl -X GET http://localhost:8000/api/exchangeRequest/last_currency/?count=3


*  Specified the number of last conversions of certain currency:
curl -X GET "http://localhost:8000/api/exchangeRequest/last_currency/?currency=cup&count=3"


You can see frontend example (http://SERVER:8000/) using BootstrapVue and VueJS components.


Steps:

1.  Download project from repo


2.  Create virtualenv and activate
virtualenv -p python3 moneyexchange
source bin/activate


3.  Go to project directory an install requirements
pip install -r requirements.txt


4.  Run server:
./manage.py runserver

